//
//  DetailViewController.h
//  Foods
//
//  Created by Cragnarsson on 2016-02-26.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Db.h"
#import "FoodProtocol.h"

@interface DetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *favouriteButton;
@property (weak, nonatomic) IBOutlet UIButton *compareButton;
@property (nonatomic) NSDictionary *myItem;
@property (nonatomic) NSString *identifierString;
@property int productNumber;
@property (nonatomic) NSString *energyValue;
@property (nonatomic) NSString *proteinValue;
@property (nonatomic) NSString *fatValue;
@property (nonatomic) NSString *carbohydratesValue;
@property (nonatomic) NSString *sugarValue;
@property (nonatomic) NSString *ratingValue;
@property (weak, nonatomic) IBOutlet UILabel *energyLabel;
@property (weak, nonatomic) IBOutlet UILabel *proteinLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatLabel;
@property (weak, nonatomic) IBOutlet UILabel *carbohydratesLabel;
@property (weak, nonatomic) IBOutlet UILabel *sugarLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;

@property (nonatomic)NSArray *searchResultArray;
@property (nonatomic)NSDictionary *dict;


@end
