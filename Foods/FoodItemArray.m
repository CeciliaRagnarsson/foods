//
//  FoodItemArray.m
//  Foods
//
//  Created by Cragnarsson on 2016-03-10.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import "FoodItemArray.h"

@implementation FoodItemArray

static FoodItemArray *_instance;

+(FoodItemArray *)getInstance{
    if(_instance == nil){
        _instance = [[FoodItemArray alloc] init];
    }
    
    return _instance;
}

-(NSMutableArray*) allFoods{
    if(_allFoods == nil){
        _allFoods = @[].mutableCopy;
    }
    
    return _allFoods;
}

-(NSMutableArray*) favouriteFoods{
    if(_favouriteFoods == nil){
        _favouriteFoods = @[].mutableCopy;
    }
    
    return _favouriteFoods;
}



@end
