//
//  FavouritesTableViewController.h
//  Foods
//
//  Created by Cragnarsson on 2016-03-03.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Db.h"
#import "FoodProtocol.h"

@interface FavouritesTableViewController : UITableViewController <UISearchResultsUpdating, myProtocol, detailProtocol>

@property UISearchController *searchController2;
@property NSArray *searchResult2;
@property (nonatomic) Db *db;


@end