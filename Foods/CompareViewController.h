//
//  CompareViewController.h
//  Foods
//
//  Created by Cragnarsson on 2016-03-22.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoodProtocol.h"

@interface CompareViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *firstItemName;
@property (weak, nonatomic) IBOutlet UILabel *secondItemName;
@property (weak, nonatomic) IBOutlet UILabel *firstItemEnergy;
@property (weak, nonatomic) IBOutlet UILabel *firstItemProtein;
@property (weak, nonatomic) IBOutlet UILabel *firstItemFat;
@property (weak, nonatomic) IBOutlet UILabel *firstItemCarbohydrates;
@property (weak, nonatomic) IBOutlet UILabel *firstItemSugar;
@property (weak, nonatomic) IBOutlet UILabel *firstItemHealth;
@property (weak, nonatomic) IBOutlet UILabel *secondItemEnergy;
@property (weak, nonatomic) IBOutlet UILabel *secondItemProtein;
@property (weak, nonatomic) IBOutlet UILabel *secondItemFat;
@property (weak, nonatomic) IBOutlet UILabel *secondItemCarbohydrates;
@property (weak, nonatomic) IBOutlet UILabel *secondItemSugar;
@property (weak, nonatomic) IBOutlet UILabel *secondItemHealth;
@property (nonatomic) NSString *firstItemNameText;
@property (nonatomic) NSString *firstItemEnergyText;
@property (nonatomic) NSString *firstItemProteinText;
@property (nonatomic) NSString *firstItemFatText;
@property (nonatomic) NSString *firstItemCarbohydratesText;
@property (nonatomic) NSString *firstItemSugarText;
@property (nonatomic) NSString *firstItemHealthText;
@property (weak, nonatomic) IBOutlet UITableView *CompareTableView;
@property (nonatomic) NSDictionary *mySecondItem;
@property (nonatomic) int productNumber;
@property (nonatomic) int secondItemProductNumber; 


@end
