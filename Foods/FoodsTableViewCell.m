//
//  FoodsTableViewCell.m
//  Foods
//
//  Created by Cragnarsson on 2016-02-26.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import "FoodsTableViewCell.h"

@implementation FoodsTableViewCell

@synthesize foodName = _foodName;
@synthesize kcalValue = _kcalValue;
@synthesize foodImage = _foodImage;

- (void)awakeFromNib {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (void)layoutSubviews {
    [super layoutSubviews];
   
     self.imageView.frame = CGRectMake(10,10,60,60);
}

@end
