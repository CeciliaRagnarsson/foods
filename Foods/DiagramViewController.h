//
//  DiagramViewController.h
//  Foods
//
//  Created by Cragnarsson on 2016-04-02.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GKBarGraph.h>

@interface DiagramViewController : UIViewController <GKBarGraphDataSource>
@property (weak, nonatomic) IBOutlet UILabel *firstItem;
@property (weak, nonatomic) IBOutlet UILabel *secondItem;
@property (nonatomic) NSString *firstItemName;
@property (nonatomic) NSString *secondItemName; 

@property (nonatomic) int firstItemFat;
@property (nonatomic) int firstItemCarbohydrates;
@property (nonatomic) int firstItemProtein;
@property (nonatomic) int secondItemFat;
@property (nonatomic) int secondItemCarbohydrates;
@property (nonatomic) int secondItemProtein;


@end
