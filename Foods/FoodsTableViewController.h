//
//  FoodsTableViewController.h
//  Foods
//
//  Created by Cragnarsson on 2016-02-26.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoodProtocol.h"
#import "Db.h"

@interface FoodsTableViewController : UITableViewController <UISearchResultsUpdating, myProtocol, detailProtocol, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property UISearchController *searchController;
@property NSArray *searchResult;
@property (nonatomic) Db *db;


@end
