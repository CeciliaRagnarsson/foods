//
//  DiagramViewController.m
//  Foods
//
//  Created by Cragnarsson on 2016-04-02.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import "DiagramViewController.h"

@interface DiagramViewController ()
@property (weak, nonatomic) IBOutlet GKBarGraph *graph;

@end

@implementation DiagramViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.graph.dataSource = self;
    [self.graph draw];
    
    self.firstItem.text = self.firstItemName;
    self.secondItem.text = self.secondItemName;
}

- (NSInteger)numberOfBars {
    return 6;
}


- (NSNumber *)valueForBarAtIndex:(NSInteger)index {
    if(index == 0) {
        return [NSNumber numberWithInt:self.firstItemFat];
    }else if(index == 1) {
        return [NSNumber numberWithInt:self.secondItemFat];
    }else if(index == 2) {
        return [NSNumber numberWithInt:self.firstItemCarbohydrates];
    }else if(index == 3) {
        return [NSNumber numberWithInt:self.secondItemCarbohydrates];
    }else if(index == 4) {
        return [NSNumber numberWithInt:self.firstItemProtein];
    }else if(index == 5) {
        return [NSNumber numberWithInt:self.secondItemProtein];
    }else {
        return @0;
    }
}


- (NSString *)titleForBarAtIndex:(NSInteger)index {
    if(index == 0  || index == 1){
        return @"F";
    }else if(index == 2 || index == 3) {
        return @"C";
    }else if(index == 4 || index == 5){
        return @"P";
    }else {
        return @"";
    }
}

- (UIColor *)colorForBarAtIndex:(NSInteger)index {
    UIColor *color1 = [UIColor colorWithRed:91.0f/255.0f green:107.0f/255.0f blue:98.0f/255.0f alpha:1.0f];
    UIColor *color2 = [UIColor colorWithRed:222.0f/255.0f green:253.0f/255.0f blue:240.0f/255.0f alpha:1.0f];
    return @[color1, color2][index % 2];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
