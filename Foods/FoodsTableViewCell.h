//
//  FoodsTableViewCell.h
//  Foods
//
//  Created by Cragnarsson on 2016-02-26.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *foodName;
@property (weak, nonatomic) IBOutlet UILabel *kcalValue;
@property (weak, nonatomic) IBOutlet UIView *foodImage;
@property (weak, nonatomic) IBOutlet UILabel *compareItemName;
@property int productNumber;

@end
