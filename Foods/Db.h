//
//  Db.h
//  Foods
//
//  Created by Cragnarsson on 2016-03-06.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FoodProtocol.h"

@interface Db : NSObject

-(void) populateFoodsArray:(NSString *) name Sender: (id <myProtocol>) sender Identifier: (int) identifier;

-(void) populateFoodsArrayWithCell:(id)cell indexPathRow:(int) row name:(NSString *) name Sender: (id <detailProtocol>) sender Identifier: (int) identifier;

@end
