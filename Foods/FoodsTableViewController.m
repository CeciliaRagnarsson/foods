//
//  FoodsTableViewController.m
//  Foods
//
//  Created by Cragnarsson on 2016-02-26.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import "FoodsTableViewController.h"
#import "FoodsTableViewCell.h"
#import "DetailViewController.h"
#import "Db.h"
#import "FoodItemArray.h"

@interface FoodsTableViewController ()

@property (nonatomic, strong)FoodItemArray *myArray;
@property (nonatomic)DetailViewController *detail;
@property (nonatomic)NSMutableArray *imagesArray;
@property (nonatomic)UIColor *color;
@property (nonatomic)NSInteger foodId;


@end

@implementation FoodsTableViewController

-(void)populate:(id)result identifier:(int)identifier{
    if(identifier == -1) {
        
    }
    
    if(identifier == 1){
        self.myArray.allFoods = result;
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
    
}

-(void)populateWithCell:(FoodsTableViewCell*)Cell indexPathRow:(int)row result:(id)result identifier:(int)identifier{
    if(identifier == -1) {
        
    }

    if(identifier == 1){
        NSDictionary *data = result[@"nutrientValues"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(Cell.tag == row + 1){
            Cell.kcalValue.text = [NSString stringWithFormat:@"Energi (Kcal): %@", data[@"energyKcal"]];
            }
        });
    }
}


- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchString = searchController.searchBar.text;
    NSPredicate *resultPredicate =
    [NSPredicate predicateWithFormat:@"name contains[c] %@", searchString];
    self.searchResult = [self.myArray.allFoods filteredArrayUsingPredicate:resultPredicate];
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.db = [[Db alloc] init];
    self.color = [UIColor colorWithRed:222.0f/255.0f green:253.0f/255.0f blue:240.0f/255.0f alpha:1.0f];
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:self.color, UITextAttributeTextColor,[UIColor whiteColor],UITextAttributeTextShadowColor,[NSValue valueWithUIOffset:UIOffsetMake(0, 1)],UITextAttributeTextShadowOffset,nil]forState:UIControlStateNormal];
    
    
    self.myArray = [FoodItemArray getInstance];
    self.imagesArray = [[NSMutableArray alloc] init];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.definesPresentationContext = YES;
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    [self.searchController loadViewIfNeeded];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.searchController.isActive && self.searchController.searchBar.text.length > 0) {
        return self.searchResult.count;
    } else {
    return [self.myArray.allFoods count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *FoodsTableIdentifier = @"Cell";
    
    FoodsTableViewCell *cell = (FoodsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:FoodsTableIdentifier forIndexPath:indexPath];
    
    if(self.myArray.allFoods.count > 0 ){
  
        cell.tag = indexPath.row + 1;
        
        if(self.searchController.isActive && self.searchController.searchBar.text.length > 0) {
            
            NSDictionary *data = self.searchResult[indexPath.row];
            NSString *getData = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", data[@"number"]];
            [self.db populateFoodsArrayWithCell:cell indexPathRow:indexPath.row name:getData Sender:self Identifier:1];
            cell.foodName.text = data [@"name"];
            NSNumber *number = data[@"number"];
            cell.productNumber = number.intValue;
            
            NSString *path = [self imagePathWithId:number];
            
            UIImage *cachedImage = [UIImage imageWithContentsOfFile:path];
            if(cachedImage) {
                cell.imageView.image = cachedImage;
                cell.imageView.layer.cornerRadius = 30;
            } else {
                cell.imageView.image = [UIImage imageNamed:@"Logo"];
            }
        }else{
            NSDictionary *data = self.myArray.allFoods[indexPath.row];
            NSString *getData = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", data[@"number"]];
            [self.db populateFoodsArrayWithCell:cell indexPathRow:indexPath.row name:getData Sender:self Identifier:1];
            cell.foodName.text = data [@"name"];
            
            NSNumber *number = data[@"number"];
            cell.productNumber = number.intValue;
         
            NSString *path = [self imagePathWithId:number];
            
            UIImage *cachedImage = [UIImage imageWithContentsOfFile:path];
            if(cachedImage) {
                cell.imageView.image = cachedImage;
                cell.imageView.layer.cornerRadius = 30;
            } else {
                cell.imageView.image = [UIImage imageNamed:@"Logo"];
            }
        }
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, 2)];
        separatorLineView.backgroundColor = [UIColor grayColor];
        [cell.contentView addSubview:separatorLineView];
        
        UIView *bgColorView = [[UIView alloc] init];
        [bgColorView setBackgroundColor:self.color];
        [cell setSelectedBackgroundView:bgColorView];
        
        UIColor *tableCellColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.1f];
        
        if(indexPath.row % 2 == 0){
            UIView *bckView = [[UIView alloc] init];
            bckView.backgroundColor = tableCellColor;
            cell.backgroundView = bckView;
        }else{
            UIView *bckView = [[UIView alloc] init];
            bckView.backgroundColor = [UIColor whiteColor];
            cell.backgroundView = bckView;
        }
         
    }
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
     
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
}
#pragma mark - Navigation



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSNumber *identifier = @(self.foodId);
    
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    NSData *imageData = UIImagePNGRepresentation(image);
    
    NSString *path = [self imagePathWithId:identifier];
    BOOL success = [imageData writeToFile:path atomically:YES];
    if(success) {
        NSLog(@"Saved image to user documents directory: %@", path);
    } else {
        NSLog(@"Failed to save image to path: %@", path);
    }
}


- (IBAction)addNewImage:(UIButton*)sender {
    
    FoodsTableViewCell *cell = sender.superview.superview;
    self.foodId = cell.productNumber;
    
    [self TakePhoto];
    
}

- (void) TakePhoto {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    [self presentViewController:imagePicker animated:YES completion:nil];
    
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
}

- (NSString*)imagePathWithId:(NSNumber *) identifier {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = paths[0];
    return [path stringByAppendingPathComponent:[NSString stringWithFormat:@"cachedImage%@.png", identifier]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"detail"]) {
        self.detail = segue.destinationViewController;
        FoodsTableViewCell *cell = sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        self.detail.title = cell.foodName.text;
        if(self.searchController.isActive && self.searchController.searchBar.text.length > 0) {
            self.detail.productNumber = cell.productNumber;
            self.detail.myItem = self.searchResult[indexPath.row];
            
        }else{
            self.detail.productNumber = cell.productNumber;
            self.detail.myItem = self.myArray.allFoods[indexPath.row];
        }
    }
}

@end
