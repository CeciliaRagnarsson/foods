//
//  CompareViewController.m
//  Foods
//
//  Created by Cragnarsson on 2016-03-22.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import "CompareViewController.h"
#import "DetailViewController.h"
#import "FoodItemArray.h"
#import "Db.h"
#import "FoodsTableViewCell.h"
#import "DiagramViewController.h"

@interface CompareViewController ()

@property (nonatomic)FoodItemArray *myArray;
@property (nonatomic)Db *db;

@end

@implementation CompareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.myArray = [FoodItemArray getInstance];
    self.db = [[Db alloc] init];
    
    self.title = @"Compare";
    self.firstItemName.text = self.firstItemNameText;
    self.firstItemEnergy.text = self.firstItemEnergyText;
    self.firstItemFat.text = self.firstItemFatText;
    self.firstItemProtein.text = self.firstItemProteinText;
    self.firstItemSugar.text = self.firstItemSugarText;
    self.firstItemCarbohydrates.text = self.firstItemCarbohydratesText;
    self.firstItemHealth.text = self.firstItemHealthText;
    
}


-(void)populateWithCell:(FoodsTableViewCell*)Cell indexPathRow:(int)row result:(id)result identifier:(int)identifier{
    if(identifier == -1) {
        
    }
    
    if(identifier == 1){
        NSDictionary *data = result[@"nutrientValues"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(Cell.tag == row + 1){
                
            }
        });
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.myArray.allFoods count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    static NSString *TableIdentifier = @"CompareListCell";
    
    FoodsTableViewCell *cell = (FoodsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:TableIdentifier forIndexPath:indexPath];
    
    if(self.myArray.allFoods.count > 0 ){
        
        cell.tag = indexPath.row +1;
        
        NSDictionary *data = self.myArray.allFoods[indexPath.row];
        NSString *getData = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", data[@"number"]];
        [self.db populateFoodsArrayWithCell:cell indexPathRow:indexPath.row name:getData Sender:self Identifier:1];
        cell.compareItemName.text = data [@"name"];
        NSNumber *number = data[@"number"];
        cell.productNumber = number.intValue;
            
        
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, 2)];
        separatorLineView.backgroundColor = [UIColor grayColor];
        [cell.contentView addSubview:separatorLineView];
        
        UIColor *selectedColor = [UIColor colorWithRed:222.0f/255.0f green:253.0f/255.0f blue:240.0f/255.0f alpha:1.0f];
        UIView *bgColorView = [[UIView alloc] init];
        [bgColorView setBackgroundColor: selectedColor];
        [cell setSelectedBackgroundView:bgColorView];
        
        UIColor *tableCellColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.1f];
        
        
        if(indexPath.row % 2 == 0){
            UIView *bckView = [[UIView alloc] init];
            bckView.backgroundColor = tableCellColor;
            cell.backgroundView = bckView;
        }else{
            UIView *bckView = [[UIView alloc] init];
            bckView.backgroundColor = [UIColor whiteColor];
            cell.backgroundView = bckView;
        }
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger selectedRow = indexPath.row+1;
    if (selectedRow || selectedRow == 0){
        self.mySecondItem = self.myArray.allFoods[indexPath.row];
        self.secondItemName.text = self.mySecondItem[@"name"];
        self.productNumber = indexPath.row +1;
        NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%d", self.productNumber];
        NSString *escaped = [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:escaped];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLSession *session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *  data, NSURLResponse * response, NSError * error) {
            if(error){
                return;
            }
            
            NSError *jsonParseError = nil;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data  options:kNilOptions error:&jsonParseError];
            
            
            if(jsonParseError){
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *data = result[@"nutrientValues"];
                
                self.secondItemEnergy.text = [NSString stringWithFormat:@"%@", data[@"energyKcal"]];
                self.secondItemProtein.text = [NSString stringWithFormat:@"%@", data[@"protein"]];
                self.secondItemFat.text = [NSString stringWithFormat:@"%@", data[@"fat"]];
                self.secondItemCarbohydrates.text = [NSString stringWithFormat:@"%@", data[@"carbohydrates"]];
                self.secondItemSugar.text = [NSString stringWithFormat:@"%@", data[@"saccharose"]];
                
                NSInteger proteinValue = [data[@"protein"] integerValue];
                NSInteger fatValue = [data[@"fat"] integerValue];
                NSInteger energyValue = [data[@"energyKcal"] integerValue];
                NSInteger sugarValue = [data[@"saccharose"] integerValue];
                
                int healthpoints = 0;
                if(sugarValue > 10){
                    self.secondItemHealth.text = @"1 / 10";
                }else{
                    if(sugarValue < 10){
                        healthpoints += 2;
                    }
                    if(fatValue < 7){
                        healthpoints += 3;
                    }
                    if(proteinValue > 10){
                        healthpoints += 3;
                    }
                    if(energyValue < 100){
                        healthpoints += 2;
                    }
                    self.secondItemHealth.text = [NSString stringWithFormat:@"%d/10", healthpoints];
                }
                
            });
            
        }];
        [task resume];
        
    }
}



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    DiagramViewController *diagram = segue.destinationViewController;
    diagram.firstItemName = self.firstItemNameText;
    diagram.secondItemName = self.secondItemName.text;
    diagram.firstItemFat = self.firstItemFatText.intValue;
    diagram.firstItemCarbohydrates = self.firstItemCarbohydratesText.intValue;
    diagram.firstItemProtein = self.firstItemProteinText.intValue;
    diagram.secondItemFat = self.secondItemFat.text.intValue;
    diagram.secondItemCarbohydrates = self.secondItemCarbohydrates.text.intValue;
    diagram.secondItemProtein = self.secondItemProtein.text.intValue;
    
}

@end
