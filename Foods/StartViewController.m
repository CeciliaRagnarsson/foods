//
//  StartViewController.m
//  Foods
//
//  Created by Cragnarsson on 2016-02-26.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import "StartViewController.h"
#import "Db.h"
#import "FoodsTableViewController.h"

@interface StartViewController ()

@property (weak, nonatomic) IBOutlet UIButton *allFoodsButton;
@property UIDynamicAnimator *animator;
@property UIGravityBehavior *gravity;
@property UICollisionBehavior *collision;
@property UISnapBehavior *snap;
@property UILabel *startLabel;

@end

@implementation StartViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIColor *color = [UIColor colorWithRed:222.0f/255.0f green:253.0f/255.0f blue:240.0f/255.0f alpha:1.0f];
    
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : color}];
    self.navigationController.navigationBar.translucent = NO;
    
    CGPoint point = CGPointMake(self.allFoodsButton.center.x, self.allFoodsButton.center.y);
    
    [self animationColor];
    
    self.startLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.allFoodsButton.center.x, 1, 250, 50)];
    self.startLabel.center = CGPointMake(point.x, 1);
    self.startLabel.text = @"My Foods!";
    self.startLabel.font = [UIFont systemFontOfSize:40];
    self.startLabel.textColor = color;
    self.startLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:self.startLabel];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.startLabel]];
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[self.startLabel, self.allFoodsButton]];
    self.snap = [[UISnapBehavior alloc] initWithItem:self.allFoodsButton snapToPoint:point];
    
    [self.gravity setMagnitude:0.1];

    [self.animator addBehavior:self.gravity];
    [self.animator addBehavior:self.collision];
    [self.animator addBehavior:self.snap];
    
    self.title = @"Foods";
}

- (void)viewDidLayoutSubviews{
    
    
}

-(void) animationColor {
    [UIView animateWithDuration:3.0 animations:^{
    [UIView setAnimationDuration:2.0];
    [UIView setAnimationDelay:1.0];
        
    self.view.backgroundColor = [UIColor lightGrayColor];
    self.startLabel.backgroundColor = [UIColor lightGrayColor];
        
    } completion:^(BOOL finished){
        
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  
    self.navigationController.navigationBar.translucent = YES;
    if([segue.identifier isEqualToString:@"AllFoods"]) {
        FoodsTableViewController *destination = [segue destinationViewController];
        Db *myDb = [[Db alloc] init];
        [myDb populateFoodsArray:@"http://matapi.se/foodstuff?nutrient" Sender:destination Identifier:1];
        
    }else if([segue.identifier isEqualToString:@"Favourites"]) {
        
    }
    
}

@end
