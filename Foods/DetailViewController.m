//
//  DetailViewController.m
//  Foods
//
//  Created by Cragnarsson on 2016-02-26.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import "DetailViewController.h"
#import "Db.h"
#import "FoodItemArray.h"
#import "FoodsTableViewController.h"
#import "CompareViewController.h"

@interface DetailViewController ()

@property (nonatomic)FoodItemArray *myArray;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.myArray = [FoodItemArray getInstance];
    
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%d", self.productNumber];
    NSString *escaped = [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:escaped];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *  data, NSURLResponse * response, NSError * error) {
        if(error){
            return;
        }
        
        NSError *jsonParseError = nil;
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data  options:kNilOptions error:&jsonParseError];
        
        
        if(jsonParseError){
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary *data = result[@"nutrientValues"];
            
            self.energyValue = [NSString stringWithFormat:@"%@", data[@"energyKcal"]];
            self.proteinValue = [NSString stringWithFormat:@"%@", data[@"protein"]];
            self.fatValue = [NSString stringWithFormat:@"%@", data[@"fat"]];
            self.carbohydratesValue = [NSString stringWithFormat:@"%@", data[@"carbohydrates"]];
            self.sugarValue = [NSString stringWithFormat:@"%@", data[@"saccharose"]];
          
            self.proteinLabel.text = self.proteinValue;
            self.energyLabel.text = self.energyValue;
            self.fatLabel.text = self.fatValue;
            self.carbohydratesLabel.text = self.carbohydratesValue;
            self.sugarLabel.text = self.sugarValue;
            
            NSInteger proteinValue = [data[@"protein"] integerValue];
            NSInteger fatValue = [data[@"fat"] integerValue];
            NSInteger energyValue = [data[@"energyKcal"] integerValue];
            NSInteger sugarValue = [data[@"saccharose"] integerValue];
            
            int healthpoints = 0;
            if(sugarValue > 10){
                self.ratingLabel.text = @"1 / 10";
            }else{
                if(sugarValue < 10){
                    healthpoints += 2;
                }
                if(fatValue < 7){
                    healthpoints += 3;
                }
                if(proteinValue > 10){
                    healthpoints += 3;
                }
                if(energyValue < 100){
                    healthpoints += 2;
                }
                self.ratingLabel.text = [NSString stringWithFormat:@"%d/10", healthpoints];
            }
            self.ratingValue = self.ratingLabel.text;
            
        });
        
    }];
    [task resume];
    
    if([self.identifierString isEqualToString:@"detailFav"]){
        self.favouriteButton.hidden = YES;
        [self.favouriteButton setEnabled: NO];
        self.compareButton.hidden = YES;
        [self.compareButton setEnabled:NO];
        
    }
    
}

- (IBAction)addFavourite:(id)sender {
    [self.myArray.favouriteFoods addObject: self.myItem];
    
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    [settings setObject: self.myArray.favouriteFoods forKey:@"myFavouritesArray"];
    [settings synchronize];
    
    
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    CompareViewController *compare = segue.destinationViewController;
    compare.firstItemNameText = self.myItem[@"name"];
    compare.firstItemEnergyText = self.energyValue;
    compare.firstItemProteinText = self.proteinValue;
    compare.firstItemSugarText = self.sugarValue;
    compare.firstItemHealthText = self.ratingValue;
    compare.firstItemFatText = self.fatValue;
    compare.firstItemCarbohydratesText = self.carbohydratesValue;
    
}


@end
