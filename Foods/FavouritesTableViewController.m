//
//  FavouritesTableViewController.m
//  Foods
//
//  Created by Cragnarsson on 2016-03-03.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import "FavouritesTableViewController.h"
#import "FoodsTableViewCell.h"
#import "DetailViewController.h"
#import "Db.h"
#import "FoodItemArray.h"

@interface FavouritesTableViewController ()

@property (nonatomic)FoodItemArray *myArray;
@property (nonatomic)UIColor *color;
@property (nonatomic)NSInteger foodId;

@end

@implementation FavouritesTableViewController

-(void)populate:(id)result identifier:(int)identifier{
    if(identifier == -1) {
        
    }
    
    if(identifier == 1){
        self.myArray.allFoods = result;
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
    
}

-(void)populateWithCell:(FoodsTableViewCell*)Cell indexPathRow:(int)row result:(id)result identifier:(int)identifier{
    if(identifier == -1) {
        
    }
    
    if(identifier == 1){
        NSDictionary *data = result[@"nutrientValues"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(Cell.tag == row){
                Cell.kcalValue.text = [NSString stringWithFormat:@"Energi (Kcal): %@", data[@"energyKcal"]];
            }
        });
    }
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchString = searchController.searchBar.text;
    NSPredicate *resultPredicate =
    [NSPredicate predicateWithFormat:@"name contains[c] %@", searchString];
    self.searchResult2 = [self.myArray.favouriteFoods filteredArrayUsingPredicate:resultPredicate];
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.color = [UIColor colorWithRed:222.0f/255.0f green:253.0f/255.0f blue:240.0f/255.0f alpha:1.0f];
 
    self.db = [[Db alloc] init];
    self.title = @"Favourite foods ❤︎";
    
    self.myArray = [FoodItemArray getInstance];
    
    self.searchController2 = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController2.searchResultsUpdater = self;
    self.searchController2.dimsBackgroundDuringPresentation = false;
    self.definesPresentationContext = YES;
    self.tableView.tableHeaderView = self.searchController2.searchBar;
    
    [self.searchController2 loadViewIfNeeded];
    
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    if(settings != nil){
        NSArray *arr = [settings objectForKey:@"myFavouritesArray"];
        self.myArray.favouriteFoods = arr.mutableCopy;
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.searchController2.isActive && self.searchController2.searchBar.text.length > 0) {
        return self.searchResult2.count;
    } else {
        return [self.myArray.favouriteFoods count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *FoodsTableIdentifier = @"FavouritesCell";
    
    FoodsTableViewCell *cell2 = (FoodsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:FoodsTableIdentifier forIndexPath:indexPath];
    
    if(self.myArray.favouriteFoods.count > 0 ){
        
        cell2.tag = indexPath.row +1;
        
        if(self.searchController2.isActive && self.searchController2.searchBar.text.length > 0) {
            NSDictionary *data = self.searchResult2[indexPath.row];
            NSString *getData = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", data[@"number"]];
            [self.db populateFoodsArrayWithCell:cell2 indexPathRow:indexPath.row name:getData Sender:self Identifier:1];
            cell2.foodName.text = data [@"name"];
            NSNumber *number = data[@"number"];
            cell2.productNumber = number.intValue;
            NSString *path = [self imagePathWithId:number];
            
            UIImage *cachedImage = [UIImage imageWithContentsOfFile:path];
            if(cachedImage) {
                NSLog(@"bild hittad at path: %@", path);
                cell2.imageView.image = cachedImage;
                cell2.imageView.layer.cornerRadius = 30;
            } else {
                cell2.imageView.image = [UIImage imageNamed:@"Logo"];
                NSLog(@"No cached image found: %@", path);
            }

        }else{
            NSDictionary *data = self.myArray.favouriteFoods[indexPath.row];
            NSString *getData = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", data[@"number"]];
            [self.db populateFoodsArrayWithCell:cell2 indexPathRow:indexPath.row name:getData Sender:self Identifier:1];
            cell2.foodName.text = data [@"name"];
            NSNumber *number = data[@"number"];
            cell2.productNumber = number.intValue;
            NSLog(@"%d", number.intValue);
            
            NSString *path = [self imagePathWithId:number];
            
            UIImage *cachedImage = [UIImage imageWithContentsOfFile:path];
            if(cachedImage) {
                NSLog(@"bild hittad at path: %@", path);
                cell2.imageView.image = cachedImage;
                cell2.imageView.layer.cornerRadius = 30;
            } else {
                cell2.imageView.image = [UIImage imageNamed:@"Logo"];
                NSLog(@"No cached image found: %@", path);
            }
        }
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cell2.frame.size.width, 2)];
        separatorLineView.backgroundColor = [UIColor grayColor];
        [cell2.contentView addSubview:separatorLineView];
        
        UIView *bgColorView = [[UIView alloc] init];
        [bgColorView setBackgroundColor:self.color];
        [cell2 setSelectedBackgroundView:bgColorView];
        
        //Change color on every other cell....
        UIColor *tableCellColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.1f];
        
        if(indexPath.row % 2 == 0){
            UIView *bckView = [[UIView alloc] init];
            bckView.backgroundColor = tableCellColor;
            cell2.backgroundView = bckView;
        }else{
            UIView *bckView = [[UIView alloc] init];
            bckView.backgroundColor = [UIColor whiteColor];
            cell2.backgroundView = bckView;
        }
        
    }

    return cell2;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self.myArray.favouriteFoods removeObjectAtIndex:indexPath.row];
        NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
        [settings setObject: self.myArray.favouriteFoods forKey:@"myFavouritesArray"];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
      
    }   
}

- (void)viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
}


//changes color on deletebutton...
/*
-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Remove" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        }];
    button.backgroundColor = [UIColor blueColor];
    
    return @[button];
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSNumber *identifier = @(self.foodId);
    
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    NSData *imageData = UIImagePNGRepresentation(image);
    
    NSString *path = [self imagePathWithId:identifier];
    BOOL success = [imageData writeToFile:path atomically:YES];
    if(success) {
        NSLog(@"Saved image to user documents directory: %@", path);
    } else {
        NSLog(@"Failed to save image to path: %@", path);
    }
}


- (IBAction)addNewImage:(UIButton*)sender {
    
    FoodsTableViewCell *cell = sender.superview.superview;
    self.foodId = cell.productNumber;
    
    [self TakePhoto];
    
}

- (void) TakePhoto {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    [self presentViewController:imagePicker animated:YES completion:nil];
    
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
}

- (NSString*)imagePathWithId:(NSNumber *) identifier {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = paths[0];
    return [path stringByAppendingPathComponent:[NSString stringWithFormat:@"cachedImage%@.png", identifier]];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"detailFav"]) {
        DetailViewController *detail = segue.destinationViewController;
        FoodsTableViewCell *cell = sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        detail.title = cell.foodName.text;
        detail.identifierString = @"detailFav";
        
        if(self.searchController2.isActive && self.searchController2.searchBar.text.length > 0) {
            detail.myItem = self.searchResult2[indexPath.row];
            detail.productNumber = cell.productNumber;
        }else{
            detail.myItem = self.myArray.favouriteFoods[indexPath.row];
            detail.productNumber = cell.productNumber;
        }
    }
}


@end
