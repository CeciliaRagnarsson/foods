//
//  FoodItemArray.h
//  Foods
//
//  Created by Cragnarsson on 2016-03-10.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FoodItemArray : NSObject

@property (nonatomic) NSMutableArray *allFoods;
@property (nonatomic) NSMutableArray *favouriteFoods;

+(FoodItemArray*) getInstance;

@end
