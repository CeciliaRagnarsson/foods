//
//  Db.m
//  Foods
//
//  Created by Cragnarsson on 2016-03-06.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import "Db.h"

@implementation Db

-(void) populateFoodsArray:(NSString *) name Sender: (id <myProtocol>) sender Identifier: (int) identifier {
    NSString *s = name;
    NSString *escaped = [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:escaped];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *  data, NSURLResponse * response, NSError * error) {
        if(error){
            return;
        }
        
        NSError *jsonParseError = nil;
        NSArray *result = [NSJSONSerialization JSONObjectWithData:data  options:kNilOptions error:&jsonParseError];
        
        if(jsonParseError){
            return;
        }
        
        [sender populate:result identifier:identifier];
    }];
    [task resume];
}

-(void) populateFoodsArrayWithCell:(id)cell indexPathRow:(int) row name:(NSString *) name Sender: (id <detailProtocol>) sender Identifier: (int) identifier {
    NSString *s = name;
    NSString *escaped = [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:escaped];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *  data, NSURLResponse * response, NSError * error) {
        if(error){
            return;
        }
        
        NSError *jsonParseError = nil;
        NSArray *result = [NSJSONSerialization JSONObjectWithData:data  options:kNilOptions error:&jsonParseError];
        
        if(jsonParseError){
            return;
        }
        
        [sender populateWithCell:cell indexPathRow:row result: result identifier:identifier];
    }];
    [task resume];
}


@end
