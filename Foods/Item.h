//
//  Item.h
//  Foods
//
//  Created by Cragnarsson on 2016-03-11.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSDictionary *identifier;
@property (nonatomic) NSDictionary *kCal;
@property (nonatomic) NSDictionary *protein;
@property (nonatomic) NSDictionary *fat;
@property (nonatomic) NSMutableArray *vitamines;

@property (nonatomic) NSMutableArray *allNutrients;

@end
