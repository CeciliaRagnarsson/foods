//
//  FoodProtocol.h
//  Foods
//
//  Created by Cragnarsson on 2016-03-10.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//


@protocol myProtocol

- (void) populate: (id)result identifier:(int)identifier;


@end


@protocol detailProtocol

- (void) populateWithCell: (id) Cell indexPathRow: (int) row result:(id)result identifier:(int)identifier;

@end
